# Chaordic

Short url application

# Build

- $ sudo add-apt-repository ppa:gophers/archive
- $ sudo apt update
- $ sudo apt-get install golang-1.9-go
- $ cp <ProjectFolder> $GOPATH/bitbucket.org/heronvitor/chaordic
- $ cd $GOPATH/bitbucket.org/heronvitor/chaordic
- $ sh install.sh

# Run

- Requirements:
  - Redis database
  - Mysql database

- User setup.sql file to create the tables in Mysql
- Configure start.sh file with databases accesses
- $ sh start.sh

# Test
- $ cd <ProjectFolder>
- $ go test ./...
