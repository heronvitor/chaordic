package cache

import (
	"testing"
)

var cache = NewRedisStore(":6379", "", "")
var hashName string

func TestSetURL(t *testing.T) {
	cache.SetURL(231, "aloalo")
}

func TestGETURL(t *testing.T) {
	url, err := cache.GetURL(231)

	if err != nil || url != "aloalo" {
		t.Error(err, "Cannot get url")
	}
}

func TestRegisterAccess(t *testing.T) {
	err := cache.RegisterAccess(231)

	if err != nil {
		t.Error(err, "Cannot register access")
	}
}

func TestGetAccessCounter(t *testing.T) {
	n, err := cache.GetAccessCounter(231)
	if err != nil {
		t.Error(err, "Cannot get access counter")
	}
	if n == 0 {
		t.Error(err, "Cannot get access counter")
	}
}

func TestGetAndDropAccess(t *testing.T) {
	var err error
	hashName, _, err = cache.GetAccesses()
	if err != nil {
		t.Error(err, "Cannot get accesses")
	}
}

func TestDropAccesses(t *testing.T) {
	err := cache.DropAccesses(hashName)
	if err != nil {
		t.Error(err, "Cannot drop accesses")
	}
	_, a, _ := cache.GetAccesses()
	if len(a) != 0 {
		t.Error(err, "Cannot drop accesses")
	}
}
