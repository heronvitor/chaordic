package cache

import (
	"strconv"
	"time"

	"github.com/garyburd/redigo/redis"
)

const accessHashName = "access"

// RedisStore provides cache
type RedisStore struct {
	Pool *redis.Pool
}

func dial(network, address, password, database string) (c redis.Conn, err error) {
	c, err = redis.Dial(network, address)
	if err != nil {
		return
	}
	if password != "" {
		if _, err = c.Do("AUTH", password); err != nil {
			c.Close()
			return
		}
	}
	if database != "" {
		if _, err := c.Do("SELECT", database); err != nil {
			c.Close()
			return nil, err
		}
	}
	return
}

// NewRedisStore creates new redis store
func NewRedisStore(address, password, database string) *RedisStore {
	pool := redis.Pool{
		MaxIdle:     3,
		IdleTimeout: 240 * time.Second,
		Dial:        func() (redis.Conn, error) { return redis.Dial("tcp", address) },
	}
	return &RedisStore{Pool: &pool}
}

// SetURL stores an association of url and id
func (s *RedisStore) SetURL(urlID int64, url string) (err error) {
	conn := s.Pool.Get()
	defer conn.Close()
	err = conn.Err()
	if err != nil {
		return
	}
	key := "url:" + strconv.FormatInt(urlID, 10)
	_, err = conn.Do("SETEX", key, 60, url)
	return err
}

// GetURL gets an url by id
func (s *RedisStore) GetURL(urlID int64) (url string, err error) {
	conn := s.Pool.Get()
	defer conn.Close()
	if err = conn.Err(); err != nil {
		return
	}
	key := "url:" + strconv.FormatInt(urlID, 10)
	data, err := conn.Do("GET", key)
	url, err = redis.String(data, err)
	return
}

// RegisterAccess updates request counter
func (s *RedisStore) RegisterAccess(urlID int64) (err error) {
	conn := s.Pool.Get()
	defer conn.Close()
	if err = conn.Err(); err != nil {
		return
	}
	key := strconv.FormatInt(urlID, 10)
	_, err = conn.Do("HINCRBY", accessHashName, key, 1)
	return
}

// GetAccessCounter increments access counter
func (s *RedisStore) GetAccessCounter(urlID int64) (n int64, err error) {
	conn := s.Pool.Get()
	defer conn.Close()
	if err = conn.Err(); err != nil {
		return
	}
	key := strconv.FormatInt(urlID, 10)
	data, err := conn.Do("HGET", accessHashName, key)
	n, err = redis.Int64(data, err)
	return
}

// GetAccesses rename the accesses hash and return its values
func (s *RedisStore) GetAccesses() (name string, accesses map[string]int64, err error) {
	conn := s.Pool.Get()
	defer conn.Close()
	if err = conn.Err(); err != nil {
		return
	}
	name = getSyncHashName()
	_, err = conn.Do("RENAME", accessHashName, name)
	if err != nil {
		return
	}
	data, err := conn.Do("HGETALL", name)
	accesses, err = redis.Int64Map(data, err)
	return
}

// DropAccesses removes the accesses hash by name
func (s *RedisStore) DropAccesses(name string) (err error) {
	conn := s.Pool.Get()
	defer conn.Close()
	if err = conn.Err(); err != nil {
		return
	}
	_, err = conn.Do("DEL", name)
	return
}

func getSyncHashName() (name string) {
	name = accessHashName + ":sync:" + strconv.FormatInt(time.Now().Unix(), 10)
	return
}

// Close closes the connection pool
func (s *RedisStore) Close() error {
	return s.Pool.Close()
}
