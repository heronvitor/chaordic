package controllers

import (
	"net/http"
	"strconv"

	"bitbucket.org/heronvitor/chaordic/cache"
	"bitbucket.org/heronvitor/chaordic/model"
	"bitbucket.org/heronvitor/chaordic/store"
	"github.com/gin-gonic/gin"
)

//URLController ...
type URLController struct {
	URLStore store.URLStore
	Cache    *cache.RedisStore
	AppURL   string
}

// NewURLController creates new URLController
func NewURLController(urlStore store.URLStore, cache *cache.RedisStore, appURL string) *URLController {
	return &URLController{
		URLStore: urlStore,
		Cache:    cache,
		AppURL:   appURL,
	}
}

// Create creates an url
func (uc *URLController) Create(c *gin.Context) {
	var url model.URL

	if err := c.ShouldBindJSON(&url); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	if err := uc.URLStore.Create(&url, c.Param("id")); err != nil {
		if err == store.ErrDuplicateKey {
			c.AbortWithStatus(http.StatusConflict)
			return
		}
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	url.SetShortURL(uc.AppURL)
	c.JSON(http.StatusCreated, url)
}

// Delete removes an url
func (uc *URLController) Delete(c *gin.Context) {
	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	if err := uc.URLStore.DeleteByID(id); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	c.String(http.StatusOK, "")
}

// GetGlobalStats -
func (uc *URLController) GetGlobalStats(c *gin.Context) {
	urserID := c.Param("id")
	urls, err := uc.URLStore.GetTopURLs(urserID)
	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	count, err := uc.URLStore.CountURLs(urserID)
	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	hits, err := uc.URLStore.CountHits(urserID)
	if err != nil {
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	if len(urls) != 0 {
		urls = model.SetShortURLs(urls, uc.AppURL)
	}
	stats := model.Stats{
		URLCount: count,
		Hits:     hits,
		TopUrls:  urls,
	}
	c.JSON(http.StatusOK, stats)
}

// RedirectToURL redirects to url associated with id
func (uc *URLController) RedirectToURL(c *gin.Context) {
	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	url, err := getURL(uc, id)
	if err != nil {
		if err == store.ErrNotFound {
			c.AbortWithStatus(http.StatusNotFound)
			return
		}
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	uc.Cache.RegisterAccess(id)
	c.Redirect(http.StatusMovedPermanently, url)
}

// GetURL gets url by id
func (uc *URLController) GetURL(c *gin.Context) {
	id, err := strconv.ParseInt(c.Param("id"), 10, 64)
	if err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	url, err := uc.URLStore.GetByID(id)
	if err != nil {
		if err == store.ErrNotFound {
			c.AbortWithStatus(http.StatusNotFound)
			return
		}
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	url.SetShortURL(uc.AppURL)
	c.JSON(http.StatusOK, url)
}

// getURL gets an url and updates cache
func getURL(uc *URLController, id int64) (url string, err error) {
	url, err = uc.Cache.GetURL(id)
	if err == nil && url != "" {
		return
	}

	u, err := uc.URLStore.GetByID(id)
	if err != nil {
		return
	}

	uc.Cache.SetURL(u.ID, u.URL)
	url = u.URL
	return
}
