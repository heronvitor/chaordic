package controllers

import (
	"net/http"

	"bitbucket.org/heronvitor/chaordic/model"
	"bitbucket.org/heronvitor/chaordic/store"
	"github.com/gin-gonic/gin"
)

//UserController ...
type UserController struct {
	UserStore store.UserStore
}

// NewUserController creates new UserController
func NewUserController(userStore store.UserStore) *UserController {
	return &UserController{UserStore: userStore}
}

// Create creates an user
func (uc *UserController) Create(c *gin.Context) {
	var user model.User

	if err := c.ShouldBindJSON(&user); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	if err := uc.UserStore.Create(&user); err != nil {
		if err == store.ErrDuplicateKey {
			c.AbortWithStatus(http.StatusConflict)
		} else {
			c.AbortWithStatus(http.StatusInternalServerError)
		}
		return
	}

	c.JSON(http.StatusCreated, user)
}

// Delete removes an user
func (uc *UserController) Delete(c *gin.Context) {
	id := c.Param("id")

	if err := uc.UserStore.DeleteByID(id); err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	c.String(http.StatusOK, "")
}
