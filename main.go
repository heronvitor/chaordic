package main

import (
	"flag"
	"os"

	"bitbucket.org/heronvitor/chaordic/cache"
	"bitbucket.org/heronvitor/chaordic/controllers"
	"bitbucket.org/heronvitor/chaordic/service"
	"bitbucket.org/heronvitor/chaordic/store"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

func setupRouter(userStore store.UserStore, urlStore store.URLStore, cache *cache.RedisStore, appURL string) *gin.Engine {

	userController := controllers.NewUserController(userStore)
	urlController := controllers.NewURLController(urlStore, cache, appURL)

	r := gin.Default()

	r.POST("/users", userController.Create)
	r.DELETE("/users/:id", userController.Delete)
	r.POST("/users/:id/urls", urlController.Create)
	r.GET("/urls/:id", urlController.RedirectToURL)
	r.DELETE("/urls/:id", urlController.Delete)
	r.GET("/users/:id/stats", urlController.GetGlobalStats)
	r.GET("/stats", urlController.GetGlobalStats)
	r.GET("/stats/:id", urlController.GetURL)

	return r
}

func main() {

	address := flag.String("address", ":8080", "Server address")
	syncInterval := flag.Int64("sync.time", 10, "Time interval betwen cache and database syncronization")
	appURL := flag.String("app.url", "http://localhost:8080", "Application url to appent to short url")
	flag.Parse()

	databaseURL := getEnv("DATABASE", "chaordic:chaordic@/chaordic")
	redAddr := getEnv("REDIS_ADDR", ":6379")
	redPass := getEnv("REDIS_PASS", "")
	redDB := getEnv("REDIS_DB", "")

	db := store.Dial(databaseURL)
	userStore := store.NewUserMsqlStore(db)
	urlStore := store.NewURLMsqlStore(db)
	cache := cache.NewRedisStore(redAddr, redPass, redDB)
	sync := service.NewSyncService(urlStore, cache)

	r := setupRouter(userStore, urlStore, cache, *appURL)

	sync.Start(*syncInterval)
	r.Run(*address)
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}
