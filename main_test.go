package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"bitbucket.org/heronvitor/chaordic/cache"
	"bitbucket.org/heronvitor/chaordic/model"
	"bitbucket.org/heronvitor/chaordic/store"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

var router *gin.Engine
var urlID int64

func init() {
	db := store.Dial("chaordic:chaordic@/chaordic")

	userStore := store.NewUserMsqlStore(db)
	urlStore := store.NewURLMsqlStore(db)
	cache := cache.NewRedisStore(":6379", "", "")

	router = setupRouter(userStore, urlStore, cache, "http://localhost:8080")
}

func TestCreateUser(t *testing.T) {
	userJSON := `{"id": "jibao"}`

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/users", strings.NewReader(userJSON))
	router.ServeHTTP(w, req)

	assert.Equal(t, 201, w.Code)
	assert.JSONEqf(t, userJSON, w.Body.String(), "error message %s", "formatted")
}

func TestCreateURL(t *testing.T) {
	userJSON := `{"url": "http://www.chaordic.com.br/folks"}`

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/users/jibao/urls", strings.NewReader(userJSON))
	router.ServeHTTP(w, req)

	assert.Equal(t, 201, w.Code)

	var url model.URL
	json.Unmarshal(w.Body.Bytes(), &url)
	urlID = url.ID
}

func TestGetURL(t *testing.T) {
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", fmt.Sprintf("/urls/%d", urlID), nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 301, w.Code)
}

func TestGetGlobalStats(t *testing.T) {
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/stats", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)

	var s model.Stats
	json.Unmarshal(w.Body.Bytes(), &s)

	assert.NotEqual(t, 0, s.URLCount)
}

func TestGetURLStats(t *testing.T) {
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", fmt.Sprintf("/stats/%d", urlID), nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)

	var url model.URL
	json.Unmarshal(w.Body.Bytes(), &url)

	assert.Equal(t, url.ID, urlID)
}

func TestGetUserStats(t *testing.T) {
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/users/jibao/stats", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)

	var s model.Stats
	json.Unmarshal(w.Body.Bytes(), &s)

	assert.NotEqual(t, 0, s.URLCount)
}

func TestDeleteURL(t *testing.T) {
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("DELETE", fmt.Sprintf("/urls/%d", urlID), nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
}

func TestDeleteUser(t *testing.T) {
	w := httptest.NewRecorder()
	req, _ := http.NewRequest("DELETE", "/users/jibao", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
}
