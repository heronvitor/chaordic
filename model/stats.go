package model

// Stats model
type Stats struct {
	Hits     int64 `json:"hits"`
	URLCount int64 `json:"urlCount"`
	TopUrls  []URL `json:"topUrls"`
}
