package model

import "fmt"

// URL model
type URL struct {
	ID       int64  `json:"id"`
	Hits     int64  `json:"hits"`
	URL      string `json:"url"`
	ShortURL string `json:"shortUrl"`
}

// SetShortURL sets shortUrl according to appAddr
func (u *URL) SetShortURL(appAddr string) {
	u.ShortURL = fmt.Sprintf("%s/urls/%d", appAddr, u.ID)
}

// SetShortURLs sets shortUrl of an array
func SetShortURLs(urls []URL, appAddr string) []URL {
	var newURLs []URL
	for _, u := range urls {
		u.SetShortURL(appAddr)
		newURLs = append(newURLs, u)
	}
	return newURLs
}
