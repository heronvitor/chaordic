package service

import (
	"strconv"
	"time"

	"bitbucket.org/heronvitor/chaordic/cache"
	"bitbucket.org/heronvitor/chaordic/store"
)

// SyncService syncronizes cache and database
type SyncService struct {
	URLStore store.URLStore
	Cache    *cache.RedisStore
	ticker   *time.Ticker
}

// NewSyncService creates new SyncService
func NewSyncService(urlStore store.URLStore, cache *cache.RedisStore) *SyncService {
	s := SyncService{
		URLStore: urlStore,
		Cache:    cache,
	}
	return &s
}

// Sync syncronizes cache and database
func (s *SyncService) Sync() {
	name, accesses, err := s.Cache.GetAccesses()
	if err != nil {
		return
	}
	for sid, hits := range accesses {
		id, err := strconv.ParseInt(sid, 10, 64)
		if err != nil {
			continue
		}
		s.URLStore.UpdateHits(id, hits)
	}
	s.Cache.DropAccesses(name)
}

// Start starts the update loop
func (s *SyncService) Start(t int64) {
	s.ticker = time.NewTicker(time.Duration(t) * time.Second)
	go func() {
		for _ = range s.ticker.C {
			s.Sync()
		}
	}()
}

// Stop loop
func (s *SyncService) Stop() {
	s.ticker.Stop()
}
