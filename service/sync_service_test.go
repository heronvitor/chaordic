package service

import (
	"testing"
	"time"

	"bitbucket.org/heronvitor/chaordic/cache"
	"bitbucket.org/heronvitor/chaordic/store"
)

var s *SyncService

func init() {
	db := store.Dial("chaordic:chaordic@/chaordic")
	urlStore := store.NewURLMsqlStore(db)
	cache := cache.NewRedisStore(":6379", "", "")
	s = NewSyncService(urlStore, cache)
}

func TestStartService(t *testing.T) {
	s.Start(1)
}

func TestStopService(t *testing.T) {
	time.Sleep(2 * time.Second)
	s.Stop()
}
