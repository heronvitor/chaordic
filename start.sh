#!/bin/bash

# Mysql database url
DATABASE=chaordic:chaordic@/chaordic

# GIN_MODE release/debug
GIN_MODE=release

# Redis access
REDIS_ADDR=:6379
REDIS_PASS=
REDIS_DB=

# Address to serve
ADDRESS=0.0.0.0:9090

# Interval to sync cache and database
SYNC_INTERVAL=5

# Url to append to shortURL
APP_URL="http://localhost:9090"


export GIN_MODE DATABASE REDIS_ADDR REDIS_PASS REDIS_DB

exec ./chaordic --sync.time ${SYNC_INTERVAL} --address ${ADDRESS} --app.url ${APP_URL}
