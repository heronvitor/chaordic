package store

import (
	"database/sql"
	"errors"
	// Mysql connector
	_ "github.com/go-sql-driver/mysql"
)

// Store errors
var (
	ErrNotFound     = errors.New("Not found")
	ErrDuplicateKey = errors.New("Duplicate key")
)

// Dial creates the database connection
func Dial(databaseURL string) *sql.DB {
	db, err := sql.Open("mysql", databaseURL)
	if err != nil {
		panic(err.Error())
	}

	if err = db.Ping(); err != nil {
		panic(err.Error())
	}
	return db
}
