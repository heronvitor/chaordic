package store

var urlStore URLStore
var userStore UserStore

func init() {
	db := Dial("chaordic:chaordic@/chaordic")
	userStore = NewUserMsqlStore(db)
	urlStore = NewURLMsqlStore(db)
}
