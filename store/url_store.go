package store

import (
	"database/sql"
	"strings"

	"bitbucket.org/heronvitor/chaordic/model"
)

// URLStore -
type URLStore interface {
	Create(u *model.URL, userID string) error
	GetByID(id int64) (model.URL, error)
	GetTopURLs(id string) ([]model.URL, error)
	CountURLs(id string) (int64, error)
	CountHits(id string) (int64, error)
	UpdateHits(id, hits int64) error
	DeleteByID(id int64) error
}

type urlMsqlStore struct {
	DB     *sql.DB
	AppURL string
}

// NewURLMsqlStore creates
func NewURLMsqlStore(db *sql.DB) URLStore {
	return &urlMsqlStore{DB: db}
}

// Create new URL
func (r *urlMsqlStore) Create(url *model.URL, userID string) (err error) {
	url.Hits = 0
	res, err := r.DB.Exec("INSERT urls SET hits=?, url=?, userId=?", &url.Hits, &url.URL, userID)
	if err != nil {
		return
	}
	id, err := res.LastInsertId()
	if err != nil {
		return
	}
	url.ID = id
	return
}

// Get url By ID
func (r *urlMsqlStore) GetByID(urlID int64) (url model.URL, err error) {
	err = r.DB.QueryRow("SELECT id, hits, url FROM urls WHERE id = ?", urlID).
		Scan(&url.ID, &url.Hits, &url.URL)

	if err == sql.ErrNoRows {
		err = ErrNotFound
	}
	return
}

// Get url By userID
func (r *urlMsqlStore) GetTopURLs(userID string) (urls []model.URL, err error) {
	var rows *sql.Rows
	if userID != "" {
		rows, err = r.DB.Query("SELECT id, hits, url FROM urls WHERE userId = ? ORDER BY hits DESC LIMIT 10", userID)
	} else {
		rows, err = r.DB.Query("SELECT id, hits, url FROM urls ORDER BY hits DESC LIMIT 10")
	}

	if err != nil {
		return
	}
	urls = make([]model.URL, 0)
	for rows.Next() {
		url := model.URL{}
		if err = rows.Scan(&url.ID, &url.Hits, &url.URL); err != nil {
			return
		}
		urls = append(urls, url)
	}
	rows.Close()
	return
}

func (r *urlMsqlStore) CountURLs(userID string) (count int64, err error) {
	if userID != "" {
		err = r.DB.QueryRow("SELECT COUNT(*) FROM urls WHERE userId = ?", userID).
			Scan(&count)
	} else {
		err = r.DB.QueryRow("SELECT COUNT(*) FROM urls").
			Scan(&count)
	}
	return
}

func (r *urlMsqlStore) CountHits(userID string) (hits int64, err error) {
	if userID != "" {
		err = r.DB.QueryRow("SELECT SUM(hits) FROM urls WHERE userId = ?", userID).
			Scan(&hits)
	} else {
		err = r.DB.QueryRow("SELECT SUM(hits) FROM urls").
			Scan(&hits)
	}

	if err != nil && strings.Contains(err.Error(), "type <nil>") {
		err = nil
		hits = 0
	}
	return
}

func (r *urlMsqlStore) UpdateHits(id, hits int64) (err error) {
	_, err = r.DB.Exec("UPDATE urls SET hits = hits + ? WHERE id = ?", hits, id)
	return
}

// Get url By ID
func (r *urlMsqlStore) DeleteByID(urlID int64) (err error) {
	_, err = r.DB.Exec("DELETE FROM urls WHERE id = ?", urlID)
	return
}
