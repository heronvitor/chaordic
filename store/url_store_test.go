package store

import (
	"testing"

	"bitbucket.org/heronvitor/chaordic/model"
)

var testURL = model.URL{
	Hits: 0,
	URL:  "www.alo.com.br",
}

var testUserURL = model.User{ID: "user1"}

func TestCreateURL(t *testing.T) {
	userStore.Create(&testUserURL)
	url := &testURL
	err := urlStore.Create(url, testUserURL.ID)
	if err != nil {
		t.Error(err, "Cannot create")
	}

	if url.ID == 0 {
		t.Error(err, "Cannot get id")
	}
}

func TestUpdateHits(t *testing.T) {
	err := urlStore.UpdateHits(testURL.ID, 10)
	if err != nil {
		t.Error(err, "Cannot get by id")
	}
}

func TestGetURLByID(t *testing.T) {
	url, err := urlStore.GetByID(testURL.ID)
	if err != nil {
		t.Error(err, "Cannot get by id")
	}

	if url.URL != testURL.URL || url.Hits < 5 || url.ID != testURL.ID {
		t.Error("cannot select url")
	}
}

func TestGetTopURLs(t *testing.T) {
	urls, err := urlStore.GetTopURLs(testUserURL.ID)
	if err != nil {
		t.Error(err, "Cannot get urls")
	}

	if len(urls) == 0 {
		t.Error("Cannot get urls")
	}
}

func TestCountURLs(t *testing.T) {
	count, err := urlStore.CountURLs(testUserURL.ID)
	if err != nil {
		t.Error(err, "Cannot count urls")
	}

	if count == 0 {
		t.Error("Cannot count urls")
	}
}

func TestCountHits(t *testing.T) {
	_, err := urlStore.CountHits(testUserURL.ID)
	if err != nil {
		t.Error(err, "Cannot hits urls")
	}
}

func TestDeleteURLByID(t *testing.T) {
	err := urlStore.DeleteByID(testURL.ID)
	if err != nil {
		t.Error(err, "Cannot delete")
	}
}
