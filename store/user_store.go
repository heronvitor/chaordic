package store

import (
	"database/sql"
	"strings"

	"bitbucket.org/heronvitor/chaordic/model"
)

// UserStore -
type UserStore interface {
	Create(user *model.User) error
	GetByID(id string) (model.User, error)
	DeleteByID(id string) error
}

type userMsqlStore struct {
	DB *sql.DB
}

// NewUserMsqlStore creates
func NewUserMsqlStore(db *sql.DB) UserStore {
	return &userMsqlStore{DB: db}
}

// Create new User
func (r *userMsqlStore) Create(user *model.User) (err error) {
	_, err = r.DB.Exec("INSERT users SET id=?", user.ID)

	if err != nil && strings.Contains(err.Error(), "Error 1062") {
		err = ErrDuplicateKey
	}
	return
}

// Get user By ID
func (r *userMsqlStore) GetByID(userID string) (user model.User, err error) {
	err = r.DB.QueryRow("SELECT id FROM users WHERE id = ?", userID).
		Scan(&user.ID)

	if err == sql.ErrNoRows {
		err = ErrNotFound
	}
	return
}

// Get user By ID
func (r *userMsqlStore) DeleteByID(userID string) (err error) {
	_, err = r.DB.Exec("DELETE FROM users WHERE id = ?", userID)
	return
}
