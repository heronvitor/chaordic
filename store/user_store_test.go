package store

import (
	"testing"

	"bitbucket.org/heronvitor/chaordic/model"
)

var testUser = model.User{
	ID: "joão",
}

func TestCreateUser(t *testing.T) {
	err := userStore.Create(&testUser)

	if err != nil {
		t.Error(err, "Cannot create")
	}
}

func TestGetUserByID(t *testing.T) {
	user, err := userStore.GetByID(testUser.ID)
	if err != nil {
		t.Error(err, "Cannot get by id")
	}

	if user.ID != testUser.ID {
		t.Error("cannot select user")
	}
}

func TestDeleteUserByID(t *testing.T) {
	user := testUser
	err := userStore.DeleteByID(user.ID)
	if err != nil {
		t.Error(err, "Cannot delete")
	}
}
